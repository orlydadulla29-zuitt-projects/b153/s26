// Introduction to Postman and REST

// When a user makes a request, the url endpoint is not the only property included in that request. There is also an HTTP method that helps determine the exact operation for the server to accomplish.

// The four HTTP methods as follows:

/*

	GET - Retrieve information about a resource
		- By default, whenever a browser loads any page, it makes a GET request.

	POST - used to create a new resource

	PUT - used to update information about an already-existing resource

	DELETE - used to delete an already-existing resource

*/


/*let http = require("http")

const server = http.createServer(function(request, response){
		
		console.log(request.method)
		
		response.writeHead(200, {'Content-Type':'text/plain'})
		
		response.end("Hello Orly!")
	
})

const port = 4000

server.listen(port)

console.log(`Server running at port ${port}`)*/


/*

Scenario: You're using a website for information about music called www.coolmusicdb.com

The first thing you want to is see a list of all the songs on the website.

You go to www.coolmusicdb.com/api/songs - The browser is able to read both the URL endpoint of the address (/api/songs) that you provided, as well as the method of request that you are making.

Since we are simply loading a page on a browser, the request that you send is automatically sent as a GET request.

The browser accepts your request, creates the request object with the following information (and more);

{
	url: "/api/songs",
	method: "GET"
}

and then shows you its entire list of songs.


*/
let http = require("http")

const server = http.createServer(function(request, response){
		
	if(request.url === '/api/products' && request.method === "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Products retrieved from database")
	} else if (request.url === '/api/products/123' && request.method === "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Product 123 retrieved from database")
	} else if (request.url === '/api/products/123' && request.method === "PUT"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Product 123 updated")
	} else if (request.url === '/api/products/123' && request.method === "DELETE"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Product 123 deleted")
	} else if (request.url === '/api/products' && request.method === "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("New product added to database")
	}

})

const port = 4000

server.listen(port)

console.log(`Server running at port ${port}`)
