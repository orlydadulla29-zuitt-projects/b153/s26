let http = require("http")

const server = http.createServer(function(request, response){
		
	if(request.url === '/api/users' && request.method === "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("All users have been listed")
	} else if (request.url === '/api/users/123' && request.method === "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("User 123 has been retrieved from database")
	} else if (request.url === '/api/users/123' && request.method === "PUT"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("User 123 updated")
	} else if (request.url === '/api/users/123' && request.method === "DELETE"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("User 123 has been deleted")
	} else if (request.url === '/api/users' && request.method === "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("You are now registered")
	}

})

const port = 4000

server.listen(port)

console.log(`Server running at port ${port}`)